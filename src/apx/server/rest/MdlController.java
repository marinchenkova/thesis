package com.novinteh.mdl.server.rest;

import com.novinteh.mdl.server.database.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author Marinchenko V. A.
 */
@RestController
public class MdlController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final RepositoryService repositoryService;


    @Autowired
    public MdlController(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }


    @GetMapping("/aomn/{aomnId:[\\d]+}")
    public CompletableFuture<String> aomnNameById(@PathVariable("aomnId") int aomnId) {
        logger.info("Requested AOMN name by ID = " + aomnId);
        return repositoryService.aomnNameById(aomnId);
    }

    @GetMapping("/aomn/list")
    public CompletableFuture<String> aomnList() {
        logger.info("Requested AOMN list");
        return repositoryService.aomnList();
    }

    @GetMapping("/regchanges/{aomnId:[\\d]+}/{prodType}/{date}")
    public CompletableFuture<String> regchangesList(@PathVariable("aomnId") int aomnId,
                                                    @PathVariable("prodType") String prodType,
                                                    @PathVariable("date") String date) {
        logger.info("Requested RegChanges list: AOMN=" + aomnId + ", " + prodType + ", " + date);
        return repositoryService.regChangesList(aomnId, prodType, date);
    }

}
