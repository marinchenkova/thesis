package com.novinteh.mdl.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * @author Marinchenko V. A.
 */
@Configuration
@EnableAsync
public class AsyncConfig {

    public static final String EXECUTOR_REPO = "repoAsyncExecutor";

    @Bean(EXECUTOR_REPO)
    public Executor repoAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(3);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("repo-async-");
        executor.initialize();
        return executor;
    }

}
