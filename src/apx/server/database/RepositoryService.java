package com.novinteh.mdl.server.database;

import com.novinteh.mdl.entity.base.Jsonable;
import com.novinteh.mdl.entity.data.Aomn;
import com.novinteh.mdl.entity.data.RegChange;
import com.novinteh.mdl.server.config.AsyncConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

/**
 * @author Marinchenko V. A.
 */
@Service
public class RepositoryService {

    private final static Logger logger = LoggerFactory.getLogger(RepositoryService.class);

    private final MapperProvider mapperProvider;
    private final JdbcTemplate jdbcTemplate;


    @Autowired
    public RepositoryService(MapperProvider mapperProvider, JdbcTemplate jdbcTemplate) {
        this.mapperProvider = mapperProvider;
        this.jdbcTemplate = jdbcTemplate;
    }


    private <R> R processWithTiming(Callable<R> callable) {
        long ms = System.currentTimeMillis();
        long dur;
        R result;

        logger.info("Start query");

        try {
            result = callable.call();
        } catch (Exception e) {
            result = null;
        }

        dur = System.currentTimeMillis() - ms;
        logger.info("Done in " + dur + " ms");

        return result;
    }

    @Async(AsyncConfig.EXECUTOR_REPO)
    public CompletableFuture<String> aomnNameById(int id) {
        String name = processWithTiming(() -> jdbcTemplate.query(
                SqlProvider.aomnNameById(id),
                mapperProvider.aomnNameResultSetExtractor()
        ));
        return CompletableFuture.completedFuture(name);

    }

    @Async(AsyncConfig.EXECUTOR_REPO)
    public CompletableFuture<String> aomnList() {
        List<Aomn> list = processWithTiming(() -> jdbcTemplate.query(
                SqlProvider.aomnList(),
                mapperProvider.aomnListMapper()
        ));
        return CompletableFuture.completedFuture(Jsonable.toJson(list));
    }

    @Async(AsyncConfig.EXECUTOR_REPO)
    public CompletableFuture<String> regChangesList(int aomnId, String prodType, String date) {
        List<RegChange> list = processWithTiming(() -> jdbcTemplate.query(
                SqlProvider.regChanges(aomnId, prodType, date),
                mapperProvider.regChangeListMapper(date)
        ));
        return CompletableFuture.completedFuture(Jsonable.toJson(list));
    }


}
