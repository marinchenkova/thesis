package com.novinteh.mdl.server.database;

/**
 * @author Marinchenko V. A.
 */
public class SqlProvider {

    public static String tehsectsByAomnId(int sourceAomnId) {
        return "select TEHSECTID, TEHSECT, SHORTNAME, PRODUCTTYPE " +
                "from MFTEHAK.TEHSECT " +
                "where ACTIVITY = 'Y' and TEHSECTID in (select TEHSECTID from MFTEHAK.PLANSCHEDULE_AK) " +
                "and SOURCEAOMNID = " + sourceAomnId +
                " order by TEHSECT";
    }


    public static String aomnList() {
        return "select * from ASKIDADM.AOMNLABEL";
    }

    public static String aomnNameById(int id) {
        return "select nvl(label.NAME, label.SHORTNAME) name\n" +
                "from ASKIDADM.AOMNLABEL label\n" +
                "where label.AOMNID = " + id + "\n" +
                "and ROWNUM = 1";
    }


    public static String regChanges(int aomnId, String prodType, String date) {
        return
                "with full as (\n" +
                        "    select\n" +
                        "        tehsect.TEHSECTID   tehsect_id,\n" +
                        "        tehsect.TEHSECT     tehsect_full_name,\n" +
                        "        tehsect.SHORTNAME   tehsect_short_Name,\n" +
                        "        nvl(tehsect.TEHSECT, tehsect.SHORTNAME) present_name,\n" +
                        "        plan.dt             plan_date,\n" +
                        "        fact.dt             fact_date,\n" +
                        "        nvl(plan.dt, fact.dt) present_date,\n" +
                        "        rank() over (partition by fact.dt order by fact.STATEDATE desc)\n" +
                        "                            last_state,\n" +
                        "        rank() over (partition by tehsect.TEHSECTID, plan.dt order by fact.dt desc)\n" +
                        "                            last_fact,\n" +
                        "        plan.FROM_REG       plan_from_reg,\n" +
                        "        plan.TO_REG         plan_to_reg,\n" +
                        "        plan.QCHANGEKIND    plan_q_kind,\n" +
                        "        plan.QPREV          plan_q_prev,\n" +
                        "        plan.QCUR           plan_q_curr,\n" +
                        "        plan.CANCELED       plan_cancelled,\n" +
                        "        fact.FROM_REG       fact_from_reg,\n" +
                        "        fact.TO_REG         fact_to_reg,\n" +
                        "        fact.QCHANGEKIND    fact_q_kind,\n" +
                        "        fact.QPREV          fact_q_prev,\n" +
                        "        fact.QCUR           fact_q_curr\n" +
                        "\n" +
                        "    from ASKIDADM.PDREGIMECHANGE plan\n" +
                        "\n" +
                        "    full join ASKIDADM.PDFACTREGIMECHANGE fact\n" +
                        "           on fact.PLANREGCHANGEID = plan.PLANREGCHANGEID\n" +
                        "\n" +
                        "    join MFTEHAK.TEHSECT tehsect\n" +
                        "      on (fact.TEHSECTID = tehsect.TEHSECTID or plan.TEHSECTID = tehsect.TEHSECTID)\n" +
                        "         and\n" +
                        "         (tehsect.PRODUCTTYPE = '" + prodType + "')\n" +
                        "         and\n" +
                        "         ((select count(struct.TEHSECTID)\n" +
                        "             from ASKIDADM.PDSTRUCTURE struct\n" +
                        "            where struct.TEHSECTID = fact.TEHSECTID\n" +
                        "               or struct.TEHSECTID = plan.TEHSECTID) > 0)\n" +
                        "\n" +
                        "    where\n" +
                        "        (plan.AOMNID = " + aomnId + " or fact.AOMNID = " + aomnId + ")\n" +
                        "        and\n" +
                        "        (plan.dt >= TO_DATE('" + date + "', 'DD.MM.YYYY') and plan.dt < (TO_DATE('" + date + "', 'DD.MM.YYYY') + 1) or fact.dt > TO_DATE('" + date + "', 'DD.MM.YYYY') and fact.dt <= (TO_DATE('" + date + "', 'DD.MM.YYYY') + 1))\n" +
                        "\n" +
                        "    order by\n" +
                        "        present_date asc,\n" +
                        "        present_name asc\n" +
                        "        " +
                        ")\n" +
                        "\n" +
                        "select\n" +
                        "    tehsect_id,\n" +
                        "    tehsect_full_name,\n" +
                        "    tehsect_short_name,\n" +
                        "    plan_date,\n" +
                        "    fact_date,\n" +
                        "    nvl(plan_from_reg, fact_from_reg) from_reg,\n" +
                        "    nvl(plan_to_reg, fact_to_reg) to_reg,\n" +
                        "    nvl(plan_q_kind, fact_q_kind) q_kind,\n" +
                        "    round(plan_q_prev) plan_q_prev,\n" +
                        "    round(plan_q_curr) plan_q_curr,\n" +
                        "    round(fact_q_prev) fact_q_prev,\n" +
                        "    round(fact_q_curr) fact_q_curr,\n" +
                        "    plan_cancelled\n" +
                        "from\n" +
                        "    full\n" +
                        "where\n" +
                        "    last_state = 1\n" +
                        "    and\n" +
                        "    (plan_date is not null and last_fact = 1 or plan_date is null)\n"
                ;
    }


}
