package com.novinteh.mdl.server.database;

import com.novinteh.mdl.entity.data.Aomn;
import com.novinteh.mdl.entity.data.QKind;
import com.novinteh.mdl.entity.data.RegChange;
import com.novinteh.mdl.entity.data.Tehsect;
import com.novinteh.mdl.entity.util.DateUtilsKt;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.util.Date;

/**
 * @author Marinchenko V. A.
 */
@Component
public class MapperProvider {

    public ResultSetExtractor<String> aomnNameResultSetExtractor() {
        return (ResultSet set) -> {
            while (set.next()) return set.getString("NAME");
            return null;
        };
    }

    public RowMapper<Aomn> aomnListMapper() {
        return (ResultSet set, int rowNum) -> {
            int aomnId = set.getInt("AOMNID");
            String name = set.getString("NAME");
            String shortName = set.getString("SHORTNAME");
            return new Aomn(aomnId, name, shortName);
        };
    }

    public RowMapper<RegChange> regChangeListMapper(String date) {
        return (ResultSet set, int rowNum) -> {
            int tehsectId = set.getInt("TEHSECT_ID");
            String fullName = set.getString("TEHSECT_FULL_NAME");
            String shortName = set.getString("TEHSECT_SHORT_NAME");
            Tehsect tehsect = new Tehsect(tehsectId, fullName, shortName);

            Date planDate = set.getDate("PLAN_DATE");
            Date factDate = set.getDate("FACT_DATE");

            Date day = DateUtilsKt.parseddMMyyyy(date);

            String fromReg = set.getString("FROM_REG");
            String toReg = set.getString("TO_REG");
            String qKindStr = set.getString("Q_KIND");
            QKind qKind;
            switch (qKindStr) {
                case "GROWTH":
                    qKind = QKind.GROWTH;
                    break;
                case "FALL":
                    qKind = QKind.FALL;
                    break;
                case "STABLE":
                    qKind = QKind.STABLE;
                    break;
                default:
                    qKind = null;
            }

            int planQPrev = set.getInt("PLAN_Q_PREV");
            int planQCurr = set.getInt("PLAN_Q_CURR");
            int factQPrev = set.getInt("FACT_Q_PREV");
            int factQCurr = set.getInt("FACT_Q_CURR");

            String planCancelledStr = set.getString("PLAN_CANCELLED");
            boolean planCancelled = "Y".equals(planCancelledStr);

            return new RegChange(
                    tehsect,
                    planDate,
                    factDate,
                    fromReg,
                    toReg,
                    qKind,
                    planQPrev,
                    planQCurr,
                    factQPrev,
                    factQCurr,
                    planCancelled,
                    day
            );
        };
    }
}
