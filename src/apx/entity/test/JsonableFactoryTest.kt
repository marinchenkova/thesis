import com.novinteh.mdl.entity.base.Jsonable
import com.novinteh.mdl.entity.data.Aomn
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.entity.data.Tehsect
import org.junit.Test

/**
 * @author Marinchenko V. A.
 */
class JsonableFactoryTest {

    private val factory = JsonableFactory()

    @Test
    fun factoryTestNoExceptions() {
        val e1 = factory.newJsonable(RegChange::class)
        val e2 = factory.newJsonable(Tehsect::class)
        val e3 = factory.newJsonable(Aomn::class)

        val l1 = factory.newJsonableList(RegChange::class)
        val l2 = factory.newJsonableList(Tehsect::class)
        val l3 = factory.newJsonableList(Aomn::class)

        factory.newJson(e1)
        factory.newJson(e2)
        factory.newJson(e3)

        factory.newJsonList(l1)
        factory.newJsonList(l2)
        factory.newJsonList(l3)
    }


}