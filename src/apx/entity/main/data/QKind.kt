package com.novinteh.mdl.entity.data

import com.google.gson.annotations.Expose

/**
 * @author Marinchenko V. A.
 */
enum class QKind(@Expose val type: String) {
    FALL("FALL"),
    GROWTH("GROWTH"),
    STABLE("STABLE")
}