package com.novinteh.mdl.entity.data

import com.google.gson.annotations.Expose

/**
 * @author Marinchenko V. A.
 */
enum class ProdType(@Expose val type: String) {
    OIL("OIL"),
    NP("NP")
}