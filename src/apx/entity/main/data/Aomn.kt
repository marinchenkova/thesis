package com.novinteh.mdl.entity.data

import com.google.gson.annotations.Expose
import com.novinteh.mdl.entity.base.Jsonable

/**
 * @author Marinchenko V. A.
 */
data class Aomn(
        @Expose val aomnId: Int,
        @Expose val name: String?,
        @Expose val shortName: String?
) : Jsonable()