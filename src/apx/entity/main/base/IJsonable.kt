package com.novinteh.mdl.entity.base

/**
 * @author Marinchenko V. A.
 */
interface IJsonable {

    fun toJson(): String?

}