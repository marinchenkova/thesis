package com.novinteh.mdl.activities

import android.os.Bundle
import android.util.Log
import com.novinteh.mdl.R
import com.novinteh.mdl.activities.base.WorkActivity
import com.novinteh.mdl.util.getAomnName
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : WorkActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initToolbar()
        initOnItemClick()
    }

    private fun initOnItemClick() {
        activity_main_reg_change_layout.setOnClickListener { startActivity<RegChangesActivity>() }
        activity_main_disp_lists_layout.setOnClickListener { startActivity<DispListsActivity>() }
        activity_main_materials_layout.setOnClickListener { startActivity<MaterialsActivity>() }
        activity_main_plan_diagram_layout.setOnClickListener { startActivity<PlanDiagramActivity>() }
    }

    override fun showProgress(show: Boolean) {}
    override fun refresh(renew: Boolean) {}
    override fun requestData(renew: Boolean) {}
}
