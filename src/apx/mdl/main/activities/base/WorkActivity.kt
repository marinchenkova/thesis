package com.novinteh.mdl.activities.base

import android.os.Bundle
import android.support.annotation.StringRes
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.novinteh.mdl.R
import com.novinteh.mdl.activities.LoginActivity
import com.novinteh.mdl.activities.RegChangesActivity
import com.novinteh.mdl.entity.data.ProdType
import com.novinteh.mdl.util.*
import kotlinx.android.synthetic.main.activity_reg_changes.*
import kotlinx.android.synthetic.main.no_data_text_center.*
import kotlinx.android.synthetic.main.progress_bar_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.startActivity


abstract class WorkActivity: ReceiverActivity() {

    private lateinit var menu: Menu
    lateinit var dater: Dater

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dater = Dater(this)
    }

    override fun initToolbar(title: String?, up: Boolean, toolbarId: Int) {
        super.initToolbar(title, up, toolbarId)
        initToolbarAomnProdTitle(title)
    }

    open fun initToolbarAomnProdTitle(title: String? = null) {
        val prefix =  if (title != null) "$title: " else ""
        val aomn = getAomnName(this)
        val prodType = pickProdType(this, getProdType(this))
        toolbar.title = "$prefix$aomn, $prodType"
    }

    override fun onResume() {
        super.onResume()
        invalidateOptionsMenu()
        swipe_refresh?.setOnRefreshListener { refresh(true) }
        swipe_refresh?.setColorSchemeResources(R.color.colorPrimary)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.prod_type, menu)
        menuInflater.inflate(R.menu.refresh, menu)
        menuInflater.inflate(R.menu.work_toolbar, menu)

        val prodType = getProdType(this)
        changeProdType(prodType, false)

        val prodTypeOil = menu.findItem(R.id.toolbar_popup_prod_type_oil)
        val prodTypeNp = menu.findItem(R.id.toolbar_popup_prod_type_np)

        if (prodType == ProdType.OIL) prodTypeOil.isChecked = true else prodTypeNp.isChecked = true

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.toolbar_popup_prod_type_oil -> {
            if (!item.isChecked) changeProdType(ProdType.OIL, true)
            item.isChecked = !item.isChecked
            true
        }
        R.id.toolbar_popup_prod_type_np -> {
            if (!item.isChecked) changeProdType(ProdType.NP, true)
            item.isChecked = !item.isChecked
            true
        }
        R.id.menu_refresh -> {
            swipe_refresh?.isRefreshing = true
            refresh(true)
            true
        }
        R.id.toolbar_popup_reg_changes -> {
            if (this !is RegChangesActivity) startActivity<RegChangesActivity>()
            true
        }
        R.id.toolbar_popup_quit -> {
            setLogged(this, false)
            startActivityClearTask<LoginActivity>()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun showProgress(show: Boolean) {
        progress_bar?.show(show)
    }

    private fun changeProdType(type: ProdType, refresh: Boolean) {
        setProdType(this, type)
        initToolbarAomnProdTitle()
        if (refresh) refresh(false)
    }

    fun displayNoData(visible: Boolean, @StringRes text: Int = R.string.no_data) {
        textview_no_data?.text = getString(text)
        textview_no_data?.show(visible)
    }

    protected abstract fun refresh(renew: Boolean)

    abstract fun requestData(renew: Boolean)

}