package com.novinteh.mdl.activities.base

import android.app.Activity
import android.support.annotation.IdRes
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import com.novinteh.mdl.R
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

/**
 * Base class for activities with toolbar.
 */
abstract class ToolbarActivity : AppCompatActivity() {

    /**
     * Initialize toolbar with [title], up navigation button ([up] is true),
     * toolbar layout resource [toolbarId]. By default up navigation is on.
     */
    open fun initToolbar(title: String? = null,
                         up: Boolean = true,
                         @IdRes toolbarId: Int = R.id.toolbar) {
        setSupportActionBar(findViewById(toolbarId))
        supportActionBar?.apply {
            this.title = title
            setDisplayHomeAsUpEnabled(up)
            setDisplayShowHomeEnabled(up)
        }
    }

    /**
     * Up navigation handler.
     */
    override fun onSupportNavigateUp(): Boolean {
        NavUtils.navigateUpFromSameTask(this)
        return true
    }

    /**
     * Start activity with clearing back stack.
     */
    inline fun <reified T : Activity> startActivityClearTask() =
        startActivity(intentFor<T>().newTask().clearTask())

}
