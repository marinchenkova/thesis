package com.novinteh.mdl.activities

import android.os.Bundle
import com.novinteh.mdl.activities.base.ToolbarActivity
import com.novinteh.mdl.util.isLogged

/**
 * Splash Activity is launcher of app with No Display theme.
 * Activity checks if user is logged in.
 * If so, go to Main Activity, else go to Login Activity.
 * Back stack is clearing while starting new activity for
 * providing proper back navigation.
 */
class SplashActivity : ToolbarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkLogged()
        finish()
    }

    /**
     * Check if user is logged in.
     */
    private fun checkLogged() {
        if (isLogged(this)) startActivityClearTask<MainActivity>()
        else startActivityClearTask<LoginActivity>()
    }
}
