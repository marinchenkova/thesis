package com.novinteh.mdl.activities

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import com.novinteh.mdl.R
import com.novinteh.mdl.activities.base.WorkActivity
import com.novinteh.mdl.adapters.SlideAdapter
import com.novinteh.mdl.fragments.RegChangesDiagramFragment
import com.novinteh.mdl.fragments.RegChangesFragment
import com.novinteh.mdl.fragments.RegChangesListFragment
import com.novinteh.mdl.services.*
import com.novinteh.mdl.util.getActivityRegChangesShowType
import com.novinteh.mdl.util.getProdType
import com.novinteh.mdl.util.setActivityRegChangesShowType
import kotlinx.android.synthetic.main.activity_reg_changes.*


class RegChangesActivity : WorkActivity(),
    DatePickerDialog.OnDateSetListener,
    SwipeRefreshLayout.OnChildScrollUpCallback {

    private var adapter: SlideAdapter? = null
    private var pagePosition = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg_changes)
        initToolbar()

        swipe_refresh?.setOnChildScrollUpCallback(this)

        activity_reg_changes_control_date?.setOnLongClickListener { onDateLongClick() }
        activity_reg_changes_control_date?.setOnClickListener { onDateClick() }
        activity_reg_changes_control_prev_date?.setOnClickListener { onPrevClick() }
        activity_reg_changes_control_next_date?.setOnClickListener { onNextClick() }

        activity_reg_changes_viewpager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {
                if (pagePosition < position) onNextClick()
                if (pagePosition > position) onPrevClick()
            }
            override fun onPageScrollStateChanged(state: Int) {
                swipe_refresh?.isEnabled =
                    state == ViewPager.SCROLL_STATE_IDLE || state == ViewPager.SCROLL_STATE_SETTLING
            }
        })
    }

    override fun canChildScrollUp(parent: SwipeRefreshLayout, child: View?): Boolean {
        return getCurrentFragment()?.canChildScrollUp() ?: true
    }

    /**
     * Toolbar menu initialization.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.show_type, menu)
        super.onCreateOptionsMenu(menu)

        val asList = getActivityRegChangesShowType(this)
        changeShowType(asList)

        val showTypeList = menu.findItem(R.id.toolbar_popup_show_type_list)
        val showTypeDiagram = menu.findItem(R.id.toolbar_popup_show_type_diagram)

        if (asList) showTypeList.isChecked = true else showTypeDiagram.isChecked = true

        return true
    }

    /**
     * Toolbar action selected.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.toolbar_popup_show_type_list -> {
            if (!item.isChecked) changeShowType(true)
            item.isChecked = !item.isChecked
            true
        }
        R.id.toolbar_popup_show_type_diagram -> {
            if (!item.isChecked) changeShowType(false)
            item.isChecked = !item.isChecked
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDateSet(datePicker: DatePicker?, year: Int, month: Int, day: Int) {
        if (dater.setDate(year, month, day)) applyDate()
    }

    private fun applyDate() {
        activity_reg_changes_control_date?.text = dater.getDateString()
        requestData(false)
        activity_reg_changes_viewpager?.setCurrentItem(1, false)
    }

    private fun onDateLongClick(): Boolean {
        if (dater.setToNow()) applyDate()
        return true
    }

    private fun onDateClick() {
        DatePickerDialog(
            this,
            this,
            dater.year(),
            dater.month(),
            dater.day()
        ).show()
    }

    private fun onPrevClick() {
        dater.setPrevDay()
        applyDate()
    }

    private fun onNextClick() {
        dater.setNextDay()
        applyDate()
    }

    private fun changeShowType(asList: Boolean) {
        setActivityRegChangesShowType(this, asList)
        changeLayout(asList)
    }

    private fun createFragment(asList: Boolean) =
        if (asList) RegChangesListFragment() else RegChangesDiagramFragment()

    private fun getCurrentFragment(): RegChangesFragment? {
        if (adapter?.middle == null) return null
        return adapter?.middle as RegChangesFragment
    }

    private fun changeLayout(asList: Boolean) {
        adapter = SlideAdapter(supportFragmentManager) { createFragment(asList) }
        activity_reg_changes_viewpager?.adapter = adapter
        activity_reg_changes_viewpager?.currentItem = 1
        applyDate()
    }

    override fun refresh(renew: Boolean) {
        swipe_refresh?.isRefreshing = renew
        requestData(renew)
    }

    override fun requestData(renew: Boolean) {
        Log.d("TIMER", "start at ${System.currentTimeMillis()}")
        displayNoData(false)
        showProgress(!renew)

        if (!renew) getCurrentFragment()?.clearTable()

        requestRegChangeDay(
            receiver,
            renew,
            getProdType(this).type,
            dater.getDateString(true)
        )
    }

    override fun onReceiveResult(code: Int, data: Bundle?) {
        when (code) {
            MSG_CODE_DATA_OK -> {
                val prod = data?.getString(MSG_KEY_PROD)
                val date = data?.getString(MSG_KEY_DATE)

                if (getProdType(this).type != prod || !dater.isToday(date)) {
                    return
                }

                showProgress(false)
                swipe_refresh?.isRefreshing = false
                val json = data.getString(MSG_KEY_DATA)
                Log.d("TIMER", "end at ${System.currentTimeMillis()}")

                getCurrentFragment()?.refreshTable(json)

                displayNoData(json.isEmpty() || json?.equals("[]") ?: true)
            }

            else -> {
                swipe_refresh?.isRefreshing = false
                super.onReceiveResult(code, data)
            }
        }
    }
}
