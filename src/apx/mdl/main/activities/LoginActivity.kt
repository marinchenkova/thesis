package com.novinteh.mdl.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.ProgressBar
import com.novinteh.mdl.R
import com.novinteh.mdl.activities.base.ReceiverActivity
import com.novinteh.mdl.activities.base.ToolbarActivity
import com.novinteh.mdl.services.*
import com.novinteh.mdl.util.*
import kotlinx.android.synthetic.main.activity_login.*


/**
 * A login screen that offers login via login/password.
 * This activity starts if user is not logged in.
 * After start of Main Activity back stack will be
 * cleared for proper back navigation.
 */
class LoginActivity : ReceiverActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initToolbar(getString(R.string.activity_login_title), false)

        // Set up the login form.
        checkCanSign()
        activity_login_login_form.afterTextChanged { checkCanSign() }
        activity_login_password_form.afterTextChanged { checkCanSign() }
        activity_login_button.setOnClickListener { attemptLogin() }
    }

    /**
     * Enable Sign in button if login and password are valid.
     */
    private fun checkCanSign() {
        activity_login_button.isEnabled =
                isLoginValid(activity_login_login_form.text.toString()) &&
                isPasswordValid(activity_login_password_form.text.toString())
    }

    /**
     * Toolbar menu initialization.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_login_toolbar, menu)
        return true
    }

    /**
     * Toolbar action selected.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.activity_login_toolbar_popup_server -> {
            showServerDialog()
            true
        }
        R.id.activity_login_toolbar_popup_aomnid -> {
            showAomnIdDialog()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    /**
     * Show aomn specifying alert dialog.
     */
    @SuppressLint("InflateParams") // AlertDialog.Builder layout does not need root view to build dialog.
    private fun showAomnIdDialog() {
        val content = layoutInflater.inflate(R.layout.aomnid_spec, null)

        // Init EditText vars on first access, when layout has been already built.
        val aomndIdText: EditText by lazy { content.findViewById<EditText>(R.id.activity_tehsect_aomnid) }

        AlertDialog.Builder(this).apply {
            setTitle("Enter AOMN ID")
            setView(content)
            setPositiveButton(R.string.ok) { dialog, _ ->
                dialog.dismiss()
                setAomnId(this@LoginActivity, aomndIdText.text.toString().toInt())
            }
        }.create().show().also { aomndIdText.setText(getAomnId(this@LoginActivity).toString()) }
    }

    /**
     * Show server specifying alert dialog to setParams server and port for login.
     */
    @SuppressLint("InflateParams") // AlertDialog.Builder layout does not need root view to build dialog.
    private fun showServerDialog() {
        val content = layoutInflater.inflate(R.layout.server_spec, null)

        // Init EditText vars on first access, when layout has been already built.
        val serverText: EditText by lazy { content.findViewById<EditText>(R.id.activity_login_server) }

        AlertDialog.Builder(this).apply {
            setTitle(R.string.activity_login_server_title)
            setView(content)
            setPositiveButton(R.string.ok) { dialog, _ ->
                setServerUrl(this@LoginActivity, serverText.text.toString())
                dialog.dismiss()
            }
        }.create().show().also {
            val serverUrl = getServerUrl(this@LoginActivity)
            serverText.setText(serverUrl)
        }
    }

    /**
     * Async login operation attempt. If error occurs, alert dialog will be shown,
     * if not, go to Main Activity.
     */
    private fun attemptLogin() {
        showProgress(true)

        if (getAomnId(this) == 0) {
            showErrorDialog(
                getString(R.string.error),
                getString(R.string.error_aomn_not_set)
            )
            return
        }

        val login = activity_login_login_form.text.toString()
        val password = activity_login_password_form.text.toString()

        RequestService.startLogin(this, receiver, login, password)
    }

    override fun showProgress(show: Boolean) {
        activity_login_button.text = if (show) getString(R.string.empty)
        else getString(R.string.activity_login_button_sign_in)

        enableLoginForms(!show)
        progress_bar?.show(show)
    }

    /**
     * Enable or disable login, password and sign in button views.
     */
    private fun enableLoginForms(enabled: Boolean) {
        activity_login_login_form.isEnabled = enabled
        activity_login_password_form.isEnabled = enabled
        activity_login_button.isEnabled = enabled
    }

    override fun onReceiveResult(code: Int, data: Bundle?) {
        when (code) {
            MSG_CODE_LOGIN_OK -> startActivityClearTask<MainActivity>()
            MSG_CODE_LOGIN_FAILED -> {
                showErrorDialog(
                    getString(R.string.error),
                    getString(R.string.error_login_failed)
                )
            }
            MSG_CODE_DATA_EMPTY -> {
                showErrorDialog(
                    getString(R.string.error),
                    getString(R.string.error_wrong_aomn)
                )
            }
            else -> super.onReceiveResult(code, data)
        }
    }
}
