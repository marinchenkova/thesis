package com.novinteh.mdl.activities

import android.os.Bundle
import android.view.Menu
import android.view.View
import com.novinteh.mdl.R
import com.novinteh.mdl.activities.base.WorkActivity
import com.novinteh.mdl.adapters.base.DATA_REG_CHANGE_DETAILS
import com.novinteh.mdl.entity.base.Jsonable
import com.novinteh.mdl.entity.base.Token
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.entity.data.RegChangeState
import com.novinteh.mdl.util.*
import kotlinx.android.synthetic.main.activity_reg_change_details.*
import kotlinx.android.synthetic.main.activity_reg_changes_list_portrait_item.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class RegChangeDetailsActivity : WorkActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg_change_details)
        initToolbar()

        showData(intent.getStringExtra(DATA_REG_CHANGE_DETAILS))
    }

    private fun showData(json: String?) {
        val regChange = try {
            Jsonable.from<RegChange>(json, Token.REG_CHANGE)
        } catch (e: Exception) {
            null
        } ?: return

        toolbar?.setBackgroundResource(regChange.backgroundRes())
        activity_reg_change_details_toolbar_back?.setBackgroundResource(regChange.backgroundRes())

        activity_reg_change_details_toolbar_date?.text = getWorkDate(this)

        activity_reg_change_details_tehsect_full?.text = regChange.tehsect.fullName ?: regChange.tehsect.shortName

        activity_reg_change_details_from_reg?.text = regChange.fromReg
        activity_reg_change_details_to_reg?.text = regChange.fromReg?.spanRegChangeCurr(regChange.toReg, resources.getColor(R.color.text_light))

        activity_reg_change_details_plan?.text = regChange.planDateString
        activity_reg_change_details_ctrl?.text = regChange.factDateString

        activity_reg_changes_table_plan_day?.show(regChange.isPlanNextDay)
        activity_reg_changes_table_ctrl_day?.show(regChange.isFactNextDay)

        if (regChange.state == RegChangeState.NOT_SCHEDULED) {
            activity_reg_change_details_plan_header?.visibility = View.GONE
            activity_reg_change_details_plan?.visibility = View.GONE
            activity_reg_change_details_q_plan_header?.visibility = View.GONE
            activity_reg_change_details_q_plan?.visibility = View.GONE
        }

        activity_reg_change_details_q_plan?.text = "${regChange.planQPrev} -> ${regChange.planQCurr}"
        activity_reg_change_details_q_fact?.text = "${regChange.factQPrev} -> ${regChange.factQCurr}"

        activity_reg_change_details_state?.text = getString(regChange.stateTextRes())
        activity_reg_change_details_state_image?.setImageResource(regChange.stateImageRes())
    }

    /**
     * Toolbar menu initialization.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.work_toolbar, menu)
        return true
    }


    override fun refresh(renew: Boolean) {

    }

    override fun requestData(renew: Boolean) {

    }


}
