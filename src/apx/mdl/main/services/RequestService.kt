package com.novinteh.mdl.services

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.os.ResultReceiver
import android.util.Log
import com.novinteh.mdl.util.*
import okhttp3.*
import org.jetbrains.anko.doAsync
import java.lang.Exception
import java.util.concurrent.TimeUnit


const val ACTION_LOGIN = "com.novinteh.mdl.services.action.LOGIN"
const val ACTION_GET_REG_CHANGE_DAY = "com.novinteh.mdl.services.action.GET_REG_CHANGE_DAY"

const val HTTP_REQUEST_TIMEOUT = 8000L
const val SERVICE_RECEIVER = "com.novinteh.mdl.services.SERVICE_RECEIVER"

const val MSG_KEY_USER = "com.novinteh.mdl.services.USER"
const val MSG_KEY_PASS = "com.novinteh.mdl.services.PASS"
const val MSG_KEY_DATE = "com.novinteh.mdl.services.DATE"
const val MSG_KEY_PROD = "com.novinteh.mdl.services.PROD"
const val MSG_KEY_DATA = "com.novinteh.mdl.services.DATA"

const val MSG_CODE_SERVER_ERROR = 1
const val MSG_CODE_LOGIN_FAILED = 2
const val MSG_CODE_LOGIN_OK = 3
const val MSG_CODE_DATA_OK = 4
const val MSG_CODE_DATA_FAILED = 5
const val MSG_CODE_DATA_EMPTY = 6


class RequestService : IntentService("RequestService") {

    private var receiver: ResultReceiver? = null
    private val okHttp = OkHttpClient().newBuilder().build()

    override fun onHandleIntent(intent: Intent?) {
        receiver = intent?.getParcelableExtra(SERVICE_RECEIVER)

        when (intent?.action) {
            ACTION_LOGIN -> {
                startLogin(intent.getStringExtra(MSG_KEY_USER), intent.getStringExtra(MSG_KEY_PASS))
            }

            ACTION_GET_REG_CHANGE_DAY -> {
                getRegChangesDay(
                    intent.getStringExtra(MSG_KEY_PROD),
                    intent.getStringExtra(MSG_KEY_DATE),
                    true
                )
            }
        }
    }

    private fun startLogin(user: String, pass: String) {
        Log.d("RequestService", "start login...")
        val aomnRequestCode = getAomnName()
        if (aomnRequestCode != MSG_CODE_DATA_OK) {
            receiver?.send(aomnRequestCode)
            return
        }

        if (!com.novinteh.mdl.net.login(user, pass)) {
            Log.d("RequestService", "login failed")
            receiver?.send(MSG_CODE_LOGIN_FAILED)
            return
        }

        Log.d("RequestService", "login ok")
        setLogged(this, true)
        receiver?.send(MSG_CODE_LOGIN_OK)
    }

    private fun getAomnName(): Int {
        Log.d("RequestService", "requesting AOMN_NAME")
        val request = mdlServer(this)
            .aomnName(this)
            .httpGet()

        val name = perform(request)

        if (name == null) {
            Log.d("RequestService", "server timeout")
            return MSG_CODE_SERVER_ERROR
        }

        if (name.isEmpty()) {
            Log.d("RequestService", "AOMN_NAME is empty")
            return MSG_CODE_DATA_EMPTY
        }

        Log.d("RequestService", "AOMN_NAME ok")
        setAomnName(this, name)
        return MSG_CODE_DATA_OK
    }

    private fun getRegChangesDay(prodType: String, date: String, sendMsg: Boolean) {
        doAsync {
            Log.d("RequestService", "requesting REG_CHANGE_DAY: $prodType, $date")

            markRequestingRegChangesDay(prodType, date)

            val request = mdlServer(this@RequestService)
                .regChangesDay(this@RequestService, date)
                .httpGet()

            val json = perform(request)

            writeRegChangesDay(prodType, date, json)

            if (json == null) {
                Log.d("RequestService", "server not reachable")
                if (sendMsg) receiver?.send(MSG_CODE_SERVER_ERROR)
                return@doAsync
            }

            Log.d("RequestService", "REG_CHANGE_DAY ok: $prodType, $date")
            if (sendMsg) receiver?.sendRegChangeDay(prodType, date, json)
        }
    }

    private fun perform(request: Request) = try {
        okHttp.newCall(request).execute().body()?.string()
    } catch (e: Exception) {
        null
    }


    companion object {
        @JvmStatic
        fun serviceIntent(ctx: Context, receiver: ServiceReceiver?) = Intent(
            ctx,
            RequestService::class.java
        ).apply {
            putExtra(SERVICE_RECEIVER, receiver)
        }

        @JvmStatic
        fun startLogin(ctx: Context, receiver: ServiceReceiver, user: String, pass: String) {
            val intent = serviceIntent(ctx, receiver).apply {
                action = ACTION_LOGIN
                putExtra(MSG_KEY_USER, user)
                putExtra(MSG_KEY_PASS, pass)
            }
            ctx.startService(intent)
        }

        @JvmStatic
        fun startGetRegChangeDay(ctx: Context, receiver: ServiceReceiver?, prodType: String, date: String) {
            val intent = serviceIntent(ctx, receiver).apply {
                action = ACTION_GET_REG_CHANGE_DAY
                putExtra(MSG_KEY_PROD, prodType)
                putExtra(MSG_KEY_DATE, date)
            }
            ctx.startService(intent)
        }
    }

}
