package com.novinteh.mdl.services

import android.content.Context
import android.util.Log
import com.novinteh.mdl.entity.data.ProdType
import com.novinteh.mdl.util.Dater
import org.jetbrains.anko.doAsync
import java.io.File


const val CACHE_REG_CHANGES_DAY = "cache-reg-changes-day"
const val CACHE_REG_CHANGE_DETAILS = "cache-reg-change-details"

const val REQUEST_MARK = "!"
const val REQUESTED_MARK = "?"


private fun getTempFile(ctx: Context, name: String, create: Boolean): File? {
    val file = File(ctx.cacheDir, name)
    if (create) file.createNewFile()
    if (!file.exists()) return null
    return file
}


private fun deleteFiles(files: Array<File>) {
    files.forEach { it.delete() }
}


fun Context.readRegChangesDay(prodType: String, date: String): String? {
    val file = getTempFile(
        this,
        "$CACHE_REG_CHANGES_DAY-$prodType-$date",
        false
    ) ?: return null
    return file.readText()
}

fun Context.markRequestingRegChangesDay(prodType: String, date: String) {
    writeRegChangesDay(prodType, date, REQUEST_MARK)
}

fun Context.writeRegChangesDay(prodType: String, date: String, data: String?) {
    Log.d("Cache", "writing REG_CHANGE_DAY: $prodType, $date, data empty: ${data?.isEmpty() ?: true}")
    getTempFile(
        this@writeRegChangesDay,
        "$CACHE_REG_CHANGES_DAY-$prodType-$date",
        true
    )?.writeText(data ?: "")
}

fun Context.clearCache() {
    doAsync {
        Log.d("Cache", "deleting all")

        val regChangesCacheFiles = cacheDir.listFiles()
        deleteFiles(regChangesCacheFiles)

    }
}
