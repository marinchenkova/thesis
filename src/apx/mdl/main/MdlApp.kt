package com.novinteh.mdl

import android.app.Application
import com.novinteh.mdl.services.clearCache
import com.novinteh.mdl.util.Dater
import com.novinteh.mdl.util.setWorkDate

class MdlApp : Application() {

    override fun onCreate() {
        super.onCreate()
        setWorkDate(this, Dater.today())
        clearCache()
    }

}