package com.novinteh.mdl.adapters

import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.novinteh.mdl.R
import com.novinteh.mdl.adapters.base.AbstractAdapter
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.entity.data.RegChangeState
import com.novinteh.mdl.util.*

class RegChangePortraitListAdapter(layoutInflater: LayoutInflater) : AbstractAdapter<RegChange>(
    layoutInflater,
    R.layout.activity_reg_changes_list_portrait_item
) {

    override fun fillView(item: RegChange, view: View?) {
        val tehsect = item.tehsect.fullName ?: item.tehsect.shortName
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_tehsect)?.text = tehsect

        val arr = canBreakRegChanges("${item.fromReg}->${item.toReg}", 4)

        view?.findViewById<TextView>(R.id.activity_reg_changes_table_change_1)?.text = arr[0]
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_change_2)?.text = arr[1]

        view?.findViewById<TextView>(R.id.activity_reg_changes_table_plan)?.text = item.planDateString
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_ctrl)?.text = item.factDateString

        view?.findViewById<TextView>(R.id.activity_reg_changes_table_plan_day)?.show(item.isPlanNextDay)
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_ctrl_day)?.show(item.isFactNextDay)

        view?.findViewById<ImageView>(R.id.activity_reg_changes_table_q_kind)?.setImageResource(item.qKindImageRes())

        view?.findViewById<View>(R.id.activity_reg_changes_side_bar)?.setBackgroundResource(item.stateBackRes())

        view?.findViewById<ImageView>(R.id.activity_reg_changes_table_state_image)?.setImageResource(item.stateImageRes())
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_state_text)?.setText(item.stateTextRes())
    }

}