package com.novinteh.mdl.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class SlideAdapter(
    manager: FragmentManager,
    private val fragment: () -> Fragment
) : FragmentStatePagerAdapter(manager) {

    var middle: Fragment? = null

    override fun getCount() = 3

    override fun getItem(position: Int): Fragment {
        val f = fragment()
        if (position == 1) middle = f
        return f
    }
}