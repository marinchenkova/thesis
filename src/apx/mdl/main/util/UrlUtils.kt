package com.novinteh.mdl.util

import android.content.Context
import android.util.Log
import okhttp3.Request
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

fun String.httpGet(): Request = Request.Builder().url(this).build()

fun mdlServer(ctx: Context) = "${getServerUrl(ctx)}/"

fun String.aomnName(ctx: Context) = this + "aomn/${getAomnId(ctx)}/"

fun String.regChangesDay(ctx: Context, date: String) = this +
        "regchanges/" +
        "${getAomnId(ctx)}/" +
        "${getProdType(ctx)}/" +
        "$date/"

fun String.test() = this + "test/"