package com.novinteh.mdl.util

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.ProdType
import kotlin.math.abs
import kotlin.math.ceil


fun String.halves(vararg delimiters: String): Array<String> {
    if (length == 1) return arrayOf(this, "")

    delimiters.forEach { if (contains(it)) return splitWithDelimiters(*delimiters) }

    val halfLen = halfLen()
    return arrayOf(substring(0, halfLen), substring(halfLen, length))
}

fun String.splitWithDelimiters(vararg d: String): Array<String> {
    val halfLen = halfLen()

    val map = mapIndexedNotNull { i, v ->
        if (d.contains(v.toString())) i to abs(i - halfLen)
        else null
    }

    val nearestDelimiter = ((map.minBy { it.second })?.first) ?: return arrayOf(this, "")
    return arrayOf(substring(0, nearestDelimiter + 1), substring(nearestDelimiter + 1, length))
}

fun String.halfLen() = ceil(length.toFloat() / 2).toInt()

fun String.spanRegChangeCurr(after: String?, color: Int = Color.argb(255, 53, 119, 166)): SpannableStringBuilder {
    val builder = SpannableStringBuilder(after)
    val list = after?.mapIndexedNotNull { i, s ->
        if (this[i] == s) null
        else i
    }

    list?.forEach {
        builder.setSpan(StyleSpan(Typeface.BOLD), it, it + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        builder.setSpan(
            ForegroundColorSpan(color),
            it,
            it + 1,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

    return builder
}

fun canBreakRegChanges(str: String?, limit: Int): Array<SpannableStringBuilder> {
    if (str == null) return arrayOf(SpannableStringBuilder(), SpannableStringBuilder())

    val array = str.halves(">")
    val prev = array[0]
    val curr = array[1]

    val builder = SpannableStringBuilder(prev)
    val spanned = prev.spanRegChangeCurr(curr)

    if (str.length > limit) {
        return arrayOf(builder, spanned)
    }

    builder.append(spanned)
    return arrayOf(builder, SpannableStringBuilder())
}

fun canBreak(str: String?, limit: Int, vararg delimiters: String): SpannableStringBuilder {
    if (str == null || str.length <= limit) return SpannableStringBuilder(str)

    val array = str.halves(*delimiters)
    val builder = SpannableStringBuilder(array[0])
    builder.appendln(array[1])
    return builder
}

fun pickAomnName(ctx: Context, full: String) = when (full) {
    ctx.getString(R.string.aomn_1_full) -> ctx.getString(R.string.aomn_1)
    ctx.getString(R.string.aomn_2_full) -> ctx.getString(R.string.aomn_2)
    ctx.getString(R.string.aomn_3_full) -> ctx.getString(R.string.aomn_3)
    ctx.getString(R.string.aomn_4_full) -> ctx.getString(R.string.aomn_4)
    ctx.getString(R.string.aomn_5_full) -> ctx.getString(R.string.aomn_5)
    ctx.getString(R.string.aomn_6_full) -> ctx.getString(R.string.aomn_6)
    ctx.getString(R.string.aomn_7_full) -> ctx.getString(R.string.aomn_7)
    ctx.getString(R.string.aomn_9_full) -> ctx.getString(R.string.aomn_9)
    ctx.getString(R.string.aomn_10_full) -> ctx.getString(R.string.aomn_10)
    ctx.getString(R.string.aomn_13_full) -> ctx.getString(R.string.aomn_13)
    ctx.getString(R.string.aomn_15_full) -> ctx.getString(R.string.aomn_15)
    ctx.getString(R.string.aomn_16_full) -> ctx.getString(R.string.aomn_16)
    ctx.getString(R.string.aomn_17_full) -> ctx.getString(R.string.aomn_17)
    ctx.getString(R.string.aomn_18_full) -> ctx.getString(R.string.aomn_18)
    ctx.getString(R.string.aomn_19_full) -> ctx.getString(R.string.aomn_19)
    ctx.getString(R.string.aomn_24_full) -> ctx.getString(R.string.aomn_24)
    ctx.getString(R.string.aomn_31350_full) -> ctx.getString(R.string.aomn_31350)
    ctx.getString(R.string.aomn_37192_full) -> ctx.getString(R.string.aomn_37192)
    else -> ctx.getString(R.string.not_set)
}

fun pickProdType(ctx: Context, type: ProdType) = when (type) {
    ProdType.OIL -> ctx.getString(R.string.prod_type_oil)
    ProdType.NP -> ctx.getString(R.string.prod_type_np)
}