package com.novinteh.mdl.util

import android.content.Context
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.ProdType
import com.novinteh.mdl.util.Prefs.PREFS_ACTIVITY_REG_CHANGES_SHOW_TYPE
import com.novinteh.mdl.util.Prefs.PREFS_AOMN_ID
import com.novinteh.mdl.util.Prefs.PREFS_AOMN_NAME
import com.novinteh.mdl.util.Prefs.PREFS_LOGGED
import com.novinteh.mdl.util.Prefs.PREFS_PROD_TYPE
import com.novinteh.mdl.util.Prefs.PREFS_SERVER_URL
import com.novinteh.mdl.util.Prefs.PREFS_WORK_DATE


private fun prefs(ctx: Context) = ctx.getSharedPreferences(Prefs.PREFS_FILENAME, 0)

fun isLogged(ctx: Context): Boolean {
    val prefs = prefs(ctx)
    return prefs.getBoolean(PREFS_LOGGED, false)
}

fun setLogged(ctx: Context, logged: Boolean) {
    val prefs = prefs(ctx)
    prefs.edit().putBoolean(Prefs.PREFS_LOGGED, logged).apply()
}

fun setServerUrl(ctx: Context, serverPort: String) {
    val prefs = prefs(ctx)
    prefs.edit().putString(PREFS_SERVER_URL, serverPort).apply()
}

fun getServerUrl(ctx: Context): String {
    val prefs = prefs(ctx)
    return prefs.getString(PREFS_SERVER_URL, "")
}

fun setProdType(ctx: Context, type: ProdType) {
    val prefs = prefs(ctx)
    prefs.edit().putString(PREFS_PROD_TYPE, type.toString()).apply()
}

fun getProdType(ctx: Context): ProdType {
    val prefs = prefs(ctx)
    return if (prefs.getString(PREFS_PROD_TYPE, ProdType.OIL.toString()) == ProdType.OIL.toString())
        ProdType.OIL
    else
        ProdType.NP
}

fun setActivityRegChangesShowType(ctx: Context, asList: Boolean) {
    val prefs = prefs(ctx)
    prefs.edit().putBoolean(PREFS_ACTIVITY_REG_CHANGES_SHOW_TYPE, asList).apply()
}

fun getActivityRegChangesShowType(ctx: Context): Boolean {
    val prefs = prefs(ctx)
    return prefs.getBoolean(PREFS_ACTIVITY_REG_CHANGES_SHOW_TYPE, true)
}

fun setAomnId(ctx: Context, aomnId: Int) {
    val prefs = prefs(ctx)
    prefs.edit().putInt(PREFS_AOMN_ID, aomnId).apply()
}

fun getAomnId(ctx: Context): Int {
    val prefs = prefs(ctx)
    return prefs.getInt(PREFS_AOMN_ID, 0)
}

fun setAomnName(ctx: Context, aomnName: String) {
    val prefs = prefs(ctx)
    prefs.edit().putString(PREFS_AOMN_NAME, pickAomnName(ctx, aomnName)).apply()
}

fun getAomnName(ctx: Context): String {
    val prefs = prefs(ctx)
    return prefs.getString(PREFS_AOMN_NAME, ctx.getString(R.string.not_set))
}

fun getWorkDate(ctx: Context): String {
    val prefs = prefs(ctx)
    return prefs.getString(PREFS_WORK_DATE, Dater.today())
}

fun setWorkDate(ctx: Context, date: String) {
    val prefs = prefs(ctx)
    prefs.edit().putString(PREFS_WORK_DATE, date).apply()
}


object Prefs {
    const val PREFS_LOGGED = "prefs_logged"
    const val PREFS_WORK_DATE = "prefs_work_date"
    const val PREFS_SERVER_URL = "prefs_server_port"
    const val PREFS_PROD_TYPE = "prefs_prod_type"
    const val PREFS_AOMN_ID = "prefs_aomn_id"
    const val PREFS_AOMN_NAME = "prefs_aomn_name"
    const val PREFS_ACTIVITY_REG_CHANGES_SHOW_TYPE = "prefs_activity_reg_changes_show_type"
    const val PREFS_FILENAME = "com.novinteh.mdl.prefs"
}