package com.novinteh.mdl.util

import android.content.Context
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

class Dater(private val ctx: Context) {

    private val calendar: Calendar = Calendar.getInstance()


    init {
        calendar.time = fullDateFormat.parse(getWorkDate(ctx))
    }

    private fun applyDate() {
        setWorkDate(ctx, fullDateFormat.format(calendar.time))
    }

    private fun setDate(date: Date): Boolean {
        if (calendar.time == date) return false
        calendar.time = date
        applyDate()
        return true
    }

    fun setDate(year: Int, month: Int, day: Int): Boolean {
        val cal = Calendar.getInstance()
        cal.set(year, month, day)
        return setDate(cal.time)
    }

    fun setToNow() = setDate(Calendar.getInstance().time)

    fun setPrevDay(): Boolean {
        val cal = Calendar.getInstance()
        cal.time = calendar.time
        cal.add(Calendar.DATE, -1)
        return setDate(cal.time)
    }

    fun setNextDay(): Boolean {
        val cal = Calendar.getInstance()
        cal.time = calendar.time
        cal.add(Calendar.DATE, +1)
        return setDate(cal.time)
    }

    fun getDateString(withYear: Boolean = false): String = getDateString(calendar, withYear)

    private fun getDateString(calendar: Calendar, withYear: Boolean = false): String = if (withYear)
        fullDateFormat.format(calendar.time)
    else ddMMDateFormat.format(calendar.time)

    fun year(): Int = calendar.get(Calendar.YEAR)
    fun month(): Int = calendar.get(Calendar.MONTH)
    fun day(): Int = calendar.get(Calendar.DAY_OF_MONTH)

    fun isYesterday(date: String?) = isDiffDate(date,-1)
    fun isTomorrow(date: String?) = isDiffDate(date,+1)
    fun isToday(date: String?) = isDiffDate(date,0)

    fun isNearToday(date: String?) = isYesterday(date) || isToday(date) || isTomorrow(date)

    private fun isDiffDate(date: String?, daysDiff: Int): Boolean {
        if (date == null) return false

        val cal = Calendar.getInstance()
        cal.time = calendar.time
        cal.add(Calendar.DATE, daysDiff)

        return date == getDateString(cal, true)
    }

    companion object {
        @JvmStatic
        val fullDateFormat = SimpleDateFormat("dd.MM.yyyy")

        @JvmStatic
        val ddMMDateFormat = SimpleDateFormat("dd.MM")

        @JvmStatic
        fun today(): String = fullDateFormat.format(Calendar.getInstance().time)

        @JvmStatic
        fun yesterday(): String {
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -1)
            return fullDateFormat.format(cal.time)
        }

        @JvmStatic
        fun tomorrow(): String {
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, +1)
            return fullDateFormat.format(cal.time)
        }
    }

}