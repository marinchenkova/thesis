package com.novinteh.mdl.util

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import com.novinteh.mdl.activities.base.ReceiverActivity
import com.novinteh.mdl.activities.base.ToolbarActivity
import com.novinteh.mdl.services.MSG_CODE_DATA_OK
import com.novinteh.mdl.services.MSG_KEY_DATA
import com.novinteh.mdl.services.MSG_KEY_DATE
import com.novinteh.mdl.services.MSG_KEY_PROD
import java.lang.ref.WeakReference

class ServiceReceiver(activity: ReceiverActivity) : ResultReceiver(Handler()) {

    private val activity = WeakReference(activity).get()

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        activity?.onReceiveResult(resultCode, resultData)
    }

}

fun ResultReceiver.send(code: Int) {
    send(code, null)
}

fun ResultReceiver.send(code: Int, key: String, data: String) {
    val bundle = Bundle()
    bundle.putString(key, data)
    send(code, bundle)
}

fun ResultReceiver.sendRegChangeDay(prodType: String, date: String, data: String) {
    val bundle = Bundle()
    bundle.putString(MSG_KEY_PROD, prodType)
    bundle.putString(MSG_KEY_DATE, date)
    bundle.putString(MSG_KEY_DATA, data)
    send(MSG_CODE_DATA_OK, bundle)
}