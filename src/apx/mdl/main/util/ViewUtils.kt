package com.novinteh.mdl.util

import android.content.res.Configuration
import android.view.View
import android.widget.ProgressBar
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.QKind
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.entity.data.RegChangeState
import org.jetbrains.anko.configuration

fun View.show(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.INVISIBLE
}

fun RegChange.stateImageRes() = when (state) {
    RegChangeState.AWAIT -> R.drawable.ic_await
    RegChangeState.DONE -> R.drawable.ic_done
    RegChangeState.LATE -> R.drawable.ic_late
    RegChangeState.FAILED -> R.drawable.ic_fail
    RegChangeState.CANCELLED -> R.drawable.ic_cancel
    RegChangeState.NOT_SCHEDULED -> R.drawable.ic_not_scheduled
    else -> R.drawable.ic_unknown_24dp
}

fun RegChange.backgroundRes() = when (state) {
    RegChangeState.AWAIT -> R.color.yellow
    RegChangeState.DONE -> R.color.green
    RegChangeState.LATE -> R.color.yellow
    RegChangeState.FAILED -> R.color.red
    RegChangeState.CANCELLED -> R.color.grey
    RegChangeState.NOT_SCHEDULED -> R.color.pink
    else -> R.color.colorPrimary
}

fun RegChange.qKindImageRes(): Int {
    return when (state) {
        RegChangeState.CANCELLED, RegChangeState.AWAIT -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_empty
            QKind.GROWTH -> R.drawable.ic_up_empty
            QKind.FALL -> R.drawable.ic_down_empty
            else -> R.drawable.ic_unknown_24dp
        }


        RegChangeState.DONE -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_green
            QKind.GROWTH -> R.drawable.ic_up_green
            QKind.FALL -> R.drawable.ic_down_green
            else -> R.drawable.ic_unknown_24dp
        }

        RegChangeState.FAILED -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_red
            QKind.GROWTH -> R.drawable.ic_up_red
            QKind.FALL -> R.drawable.ic_down_red
            else -> R.drawable.ic_unknown_24dp
        }

        RegChangeState.NOT_SCHEDULED -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_pink
            QKind.GROWTH -> R.drawable.ic_up_pink
            QKind.FALL -> R.drawable.ic_down_pink
            else -> R.drawable.ic_unknown_24dp
        }

        RegChangeState.LATE -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_yellow
            QKind.GROWTH -> R.drawable.ic_up_yellow
            QKind.FALL -> R.drawable.ic_down_yellow
            else -> R.drawable.ic_unknown_24dp
        }

        else -> R.drawable.ic_unknown_24dp
    }
}

fun RegChange.stateBackRes() = when (state) {
    RegChangeState.AWAIT -> R.drawable.side_bar_yellow
    RegChangeState.DONE -> R.drawable.side_bar_green
    RegChangeState.LATE -> R.drawable.side_bar_yellow
    RegChangeState.FAILED -> R.drawable.side_bar_red
    RegChangeState.CANCELLED -> R.drawable.side_bar_grey
    RegChangeState.NOT_SCHEDULED -> R.drawable.side_bar_pink
    else -> R.drawable.side_bar_grey
}

fun RegChange.stateTextRes() = when (state) {
    RegChangeState.AWAIT -> R.string.reg_change_state_await
    RegChangeState.DONE -> R.string.reg_change_state_done
    RegChangeState.LATE -> R.string.reg_change_state_late
    RegChangeState.FAILED -> R.string.reg_change_state_failed
    RegChangeState.CANCELLED -> R.string.reg_change_state_cancelled
    RegChangeState.NOT_SCHEDULED -> R.string.reg_change_state_not_scheduled
    else -> R.string.reg_change_state_unknown
}

fun Configuration.isLandscape() = (orientation == Configuration.ORIENTATION_LANDSCAPE)
fun Configuration.isPortrait() = orientation == Configuration.ORIENTATION_PORTRAIT

