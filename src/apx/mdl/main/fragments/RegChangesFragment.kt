package com.novinteh.mdl.fragments

import android.os.Bundle
import android.view.View
import com.novinteh.mdl.activities.RegChangeDetailsActivity
import com.novinteh.mdl.adapters.base.DATA_REG_CHANGE_DETAILS
import com.novinteh.mdl.entity.base.Jsonable
import com.novinteh.mdl.entity.base.Token
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.fragments.base.RefreshFragment
import com.novinteh.mdl.util.isLandscape
import kotlinx.android.synthetic.main.activity_reg_changes_list_portrait.view.*
import org.jetbrains.anko.configuration
import org.jetbrains.anko.support.v4.startActivity
import java.lang.Exception

abstract class RegChangesFragment : RefreshFragment<RegChange>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = if (activity?.configuration?.isLandscape() == true) landscapeAdapter() else portraitAdapter()

        listView = view.reg_changes_list_view
        listView?.adapter = adapter
        listView?.setOnItemClickListener { _, _, position, _ ->
            startActivity<RegChangeDetailsActivity>(
                DATA_REG_CHANGE_DETAILS to (adapter?.getItem(position)?.toJson() ?: "")
            )
        }
    }

    override fun refreshTable(json: String?) {
        val list = try {
            Jsonable.listFrom<RegChange>(json, Token.REG_CHANGE_LIST).filterNotNull()
        } catch (e: Exception) {
            null
        } ?: return
        adapter?.renew(list)
    }

}