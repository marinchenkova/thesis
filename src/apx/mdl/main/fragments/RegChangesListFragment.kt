package com.novinteh.mdl.fragments

import android.content.res.Configuration
import com.novinteh.mdl.R
import com.novinteh.mdl.adapters.RegChangePortraitListAdapter
import com.novinteh.mdl.util.isLandscape


class RegChangesListFragment : RegChangesFragment() {

    override fun layout(config: Configuration?): Int =
        if (config?.isLandscape() == true)
            R.layout.activity_reg_changes_list_landscape
        else R.layout.activity_reg_changes_list_portrait

    override fun landscapeAdapter() =
        RegChangePortraitListAdapter(layoutInflater)

    override fun portraitAdapter() =
        RegChangePortraitListAdapter(layoutInflater)
}