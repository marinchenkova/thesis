package com.novinteh.mdl.fragments.base

import android.content.res.Configuration
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ListView
import com.novinteh.mdl.adapters.base.AbstractAdapter
import com.novinteh.mdl.entity.data.RegChange
import org.jetbrains.anko.configuration

abstract class RefreshFragment<T> : Fragment() {

    @LayoutRes var layout: Int = 0
    protected var adapter: AbstractAdapter<T>? = null
    protected var listView: AbsListView? = null
    
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        layout = layout(activity?.configuration)
        return inflater.inflate(
            layout,
            container,
            false
        )
    }

    fun canChildScrollUp(): Boolean {
        return listView?.canScrollVertically(-1) ?: false
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        activity?.layoutInflater?.inflate(layout(newConfig), null)
    }

    fun clearTable() {
        adapter?.clear()
    }

    abstract fun refreshTable(json: String?)
    protected abstract fun layout(config: Configuration?): Int
    protected abstract fun landscapeAdapter(): AbstractAdapter<T>
    protected abstract fun portraitAdapter(): AbstractAdapter<T>
}