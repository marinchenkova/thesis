abstract class RefreshFragment<T> : Fragment() {

    protected var adapter: AbstractAdapter<T>? = null
    protected var listView: AbsListView? = null

    fun clearTable() {
        adapter?.clear()
    }

    abstract fun refreshTable(json: String?)
    protected abstract fun layout(config: Configuration?): Int
    protected abstract fun landscapeAdapter(): AbstractAdapter<T>
    protected abstract fun portraitAdapter(): AbstractAdapter<T>

    ...
}