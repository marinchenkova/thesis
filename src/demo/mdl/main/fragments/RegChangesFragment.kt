abstract class RegChangesFragment : RefreshFragment<RegChange>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = if (activity?.configuration?.isLandscape() == true) landscapeAdapter() else portraitAdapter()

        listView = view.reg_changes_list_view
        listView?.adapter = adapter
        listView?.setOnItemClickListener { _, _, position, _ ->
            startActivity<RegChangeDetailsActivity>(
                DATA_REG_CHANGE_DETAILS to (adapter?.getItem(position)?.toJson() ?: "")
            )
        }
    }

    override fun refreshTable(json: String?) {
        val list = try {
            Jsonable.listFrom<RegChange>(json, Token.REG_CHANGE_LIST).filterNotNull()
        } catch (e: Exception) {
            null
        } ?: return
        adapter?.renew(list)
    }

}