class RegChangePortraitListAdapter(layoutInflater: LayoutInflater) : AbstractAdapter<RegChange>(
    layoutInflater,
    R.layout.activity_reg_changes_list_portrait_item
) {

    override fun fillView(item: RegChange, view: View?) {
        val tehsect = item.tehsect.fullName ?: item.tehsect.shortName
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_tehsect)?.text = tehsect

        val arr = canBreakRegChanges("${item.fromReg}->${item.toReg}", 4)
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_change_1)?.text = arr[0]
        view?.findViewById<TextView>(R.id.activity_reg_changes_table_change_2)?.text = arr[1]
        ...
    }

}