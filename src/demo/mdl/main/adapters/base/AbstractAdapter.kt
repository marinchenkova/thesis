abstract class AbstractAdapter<T>(
    private val inflater: LayoutInflater,
    @LayoutRes private val itemLayout: Int
) : BaseAdapter() {

    private val list = ArrayList<T>()

    override fun getItem(position: Int): T = list[position]
    override fun getItemId(position: Int): Long = position.toLong()
    override fun getCount(): Int = list.size

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var view = convertView
        if (view == null) view = inflater.inflate(itemLayout, parent, false)
        fillView(getItem(position), view)
        return view
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    fun renew(new: List<T>) {
        list.clear()
        list.addAll(new)
        notifyDataSetChanged()
    }

    protected abstract fun fillView(item: T, view: View?)
}
