class RequestService : IntentService("RequestService") {

    private var receiver: ResultReceiver? = null
    private val okHttp = OkHttpClient().newBuilder().build()

    override fun onHandleIntent(intent: Intent?) {
        receiver = intent?.getParcelableExtra(SERVICE_RECEIVER)

        when (intent?.action) {
            ACTION_LOGIN -> {
                startLogin(intent.getStringExtra(MSG_KEY_USER), intent.getStringExtra(MSG_KEY_PASS))
            }

            ACTION_GET_REG_CHANGE_DAY -> {
                getRegChangesDay(
                    intent.getStringExtra(MSG_KEY_PROD),
                    intent.getStringExtra(MSG_KEY_DATE),
                    true
                )
            }
        }
    }

    private fun startLogin(user: String, pass: String) {
        Log.d("RequestService", "start login...")
        val aomnRequestCode = getAomnName()
        if (aomnRequestCode != MSG_CODE_DATA_OK) {
            receiver?.send(aomnRequestCode)
            return
        }

        if (!com.novinteh.mdl.net.login(user, pass)) {
            Log.d("RequestService", "login failed")
            receiver?.send(MSG_CODE_LOGIN_FAILED)
            return
        }

        Log.d("RequestService", "login ok")
        setLogged(this, true)
        receiver?.send(MSG_CODE_LOGIN_OK)
    }

    private fun getAomnName(): Int {
        Log.d("RequestService", "requesting AOMN_NAME")
        val request = mdlServer(this)
            .aomnName(this)
            .httpGet()

        val name = perform(request)

        if (name == null) {
            Log.d("RequestService", "server timeout")
            return MSG_CODE_SERVER_ERROR
        }

        if (name.isEmpty()) {
            Log.d("RequestService", "AOMN_NAME is empty")
            return MSG_CODE_DATA_EMPTY
        }

        Log.d("RequestService", "AOMN_NAME ok")
        setAomnName(this, name)
        return MSG_CODE_DATA_OK
    }

    private fun getRegChangesDay(prodType: String, date: String, sendMsg: Boolean) {
        doAsync {
            Log.d("RequestService", "requesting REG_CHANGE_DAY: $prodType, $date")

            markRequestingRegChangesDay(prodType, date)

            val request = mdlServer(this@RequestService)
                .regChangesDay(this@RequestService, date)
                .httpGet()

            val json = perform(request)

            writeRegChangesDay(prodType, date, json)

            if (json == null) {
                Log.d("RequestService", "server not reachable")
                if (sendMsg) receiver?.send(MSG_CODE_SERVER_ERROR)
                return@doAsync
            }

            Log.d("RequestService", "REG_CHANGE_DAY ok: $prodType, $date")
            if (sendMsg) receiver?.sendRegChangeDay(prodType, date, json)
        }
    }

    private fun perform(request: Request) = try {
        okHttp.newCall(request).execute().body()?.string()
    } catch (e: Exception) {
        null
    }

    ...
}
