fun Context.requestRegChangeDay(receiver: ServiceReceiver?,
                                renew: Boolean,
                                prodType: String,
                                date: String) {
    doAsync {
        if (renew) {
            Log.d("DataProvider", "renew REG_CHANGE_DAY: $prodType, $date")
            RequestService.startGetRegChangeDay(this@requestRegChangeDay, receiver, prodType, date)
            return@doAsync
        }

        Log.d("DataProvider", "requesting local REG_CHANGE_DAY: $prodType, $date")
        val localData = readRegChangesDay(prodType, date)

        if (localData == REQUEST_MARK) {
            Log.d("DataProvider", "already requesting REG_CHANGE_DAY: $prodType, $date")
            return@doAsync
        }

        if (localData == null || localData.isEmpty()) {
            Log.d("DataProvider", "not found local REG_CHANGE_DAY: $prodType, $date")
            RequestService.startGetRegChangeDay(this@requestRegChangeDay, receiver, prodType, date)
            return@doAsync
        }

        Log.d("DataProvider", "found local REG_CHANGE_DAY: $prodType, $date")
        receiver?.sendRegChangeDay(prodType, date, localData)
    }
}