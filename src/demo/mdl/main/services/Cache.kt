fun Context.readRegChangesDay(prodType: String, date: String): String? {
    val file = getTempFile(
        this,
        "$CACHE_REG_CHANGES_DAY-$prodType-$date",
        false
    ) ?: return null
    return file.readText()
}

fun Context.writeRegChangesDay(prodType: String, date: String, data: String?) {
    Log.d("Cache", "writing REG_CHANGE_DAY: $prodType, $date, data empty: ${data?.isEmpty() ?: true}")
    getTempFile(
        this@writeRegChangesDay,
        "$CACHE_REG_CHANGES_DAY-$prodType-$date",
        true
    )?.writeText(data ?: "")
}

fun Context.clearCache() {
    doAsync {
        Log.d("Cache", "deleting all")
        val regChangesCacheFiles = cacheDir.listFiles()
        deleteFiles(regChangesCacheFiles)
    }
}

...