class LoginActivity : ReceiverActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initToolbar(getString(R.string.activity_login_title), false)

        // Set up the login form.
        checkCanSign()
        activity_login_login_form.afterTextChanged { checkCanSign() }
        activity_login_password_form.afterTextChanged { checkCanSign() }
        activity_login_button.setOnClickListener { attemptLogin() }
    }

    ...

    override fun onReceiveResult(code: Int, data: Bundle?) {
        when (code) {
            MSG_CODE_LOGIN_OK -> startActivityClearTask<MainActivity>()
            MSG_CODE_LOGIN_FAILED -> {
                showErrorDialog(
                    getString(R.string.error),
                    getString(R.string.error_login_failed)
                )
            }
            MSG_CODE_DATA_EMPTY -> {
                showErrorDialog(
                    getString(R.string.error),
                    getString(R.string.error_wrong_aomn)
                )
            }
            else -> super.onReceiveResult(code, data)
        }
    }
}
