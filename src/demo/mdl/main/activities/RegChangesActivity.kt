class RegChangesActivity : WorkActivity(),
    DatePickerDialog.OnDateSetListener,
    SwipeRefreshLayout.OnChildScrollUpCallback {

    override fun onReceiveResult(code: Int, data: Bundle?) {
        when (code) {
            MSG_CODE_DATA_OK -> {
                val prod = data?.getString(MSG_KEY_PROD)
                val date = data?.getString(MSG_KEY_DATE)

                if (getProdType(this).type != prod || !dater.isToday(date)) {
                    return
                }

                showProgress(false)
                swipe_refresh?.isRefreshing = false
                val json = data.getString(MSG_KEY_DATA)
                Log.d("TIMER", "end at ${System.currentTimeMillis()}")

                getCurrentFragment()?.refreshTable(json)

                displayNoData(json.isEmpty() || json?.equals("[]") ?: true)
            }

            else -> {
                swipe_refresh?.isRefreshing = false
                super.onReceiveResult(code, data)
            }
        }
    }

    ...
}
