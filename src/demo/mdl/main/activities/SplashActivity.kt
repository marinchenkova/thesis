class SplashActivity : ToolbarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkLogged()
        finish()
    }

    private fun checkLogged() {
        if (isLogged(this)) startActivityClearTask<MainActivity>()
        else startActivityClearTask<LoginActivity>()
    }
}
