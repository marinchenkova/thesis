package com.novinteh.mdl.activities.base

import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.novinteh.mdl.R
import com.novinteh.mdl.services.MSG_CODE_SERVER_ERROR
import com.novinteh.mdl.util.ServiceReceiver

abstract class ReceiverActivity : ToolbarActivity() {

    lateinit var receiver: ServiceReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        receiver = ServiceReceiver(this)
    }

    open fun onReceiveResult(code: Int, data: Bundle?) {
        when (code) {
            MSG_CODE_SERVER_ERROR -> {
                showErrorDialog(
                    getString(R.string.error),
                    getString(R.string.error_server_not_reachable)
                )
            }
        }
    }

    ...
}