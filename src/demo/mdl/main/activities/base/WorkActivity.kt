abstract class WorkActivity: ReceiverActivity() {

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_refresh -> {
            swipe_refresh?.isRefreshing = true
            refresh(true)
            true
        }
        R.id.toolbar_popup_reg_changes -> {
            if (this !is RegChangesActivity) startActivity<RegChangesActivity>()
            true
        }
        R.id.toolbar_popup_quit -> {
            setLogged(this, false)
            startActivityClearTask<LoginActivity>()
            true
        }
         
        ...

        else -> super.onOptionsItemSelected(item)
    }

    protected abstract fun refresh(renew: Boolean)

    abstract fun requestData(renew: Boolean)

    ...
}