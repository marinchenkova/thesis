class RegChangeDetailsActivity : WorkActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg_change_details)
        initToolbar()

        showData(intent.getStringExtra(DATA_REG_CHANGE_DETAILS))
    }

    private fun showData(json: String?) {
        val regChange = try {
            Jsonable.from<RegChange>(json, Token.REG_CHANGE)
        } catch (e: Exception) {
            null
        } ?: return

        
        activity_reg_change_details_toolbar_date?.text = getWorkDate(this)
        activity_reg_change_details_tehsect_full?.text = regChange.tehsect.fullName ?: regChange.tehsect.shortName
        ...
    }
    
    ...
}
