class MdlApp : Application() {

    override fun onCreate() {
        super.onCreate()
        setWorkDate(this, Dater.today())
        clearCache()
    }

}