class ServiceReceiver(activity: ReceiverActivity) : ResultReceiver(Handler()) {

    private val activity = WeakReference(activity).get()

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        activity?.onReceiveResult(resultCode, resultData)
    }

}