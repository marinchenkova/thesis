fun isLogged(ctx: Context): Boolean {
    val prefs = prefs(ctx)
    return prefs.getBoolean(PREFS_LOGGED, false)
}

fun setLogged(ctx: Context, logged: Boolean) {
    val prefs = prefs(ctx)
    prefs.edit().putBoolean(Prefs.PREFS_LOGGED, logged).apply()
}

...