@Component
public class MapperProvider {

    public RowMapper<Aomn> aomnListMapper() {
        return (ResultSet set, int rowNum) -> {
            int aomnId = set.getInt("AOMNID");
            String name = set.getString("NAME");
            String shortName = set.getString("SHORTNAME");
            return new Aomn(aomnId, name, shortName);
        };
    }

    ...

}
