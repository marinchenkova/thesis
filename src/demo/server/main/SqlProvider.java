public class SqlProvider {

    public static String tehsectsByAomnId(int sourceAomnId) {
        return "select TEHSECTID, TEHSECT, SHORTNAME, PRODUCTTYPE " +
                "from MFTEHAK.TEHSECT " +
                "where ACTIVITY = 'Y' and TEHSECTID in (select TEHSECTID from MFTEHAK.PLANSCHEDULE_AK) " +
                "and SOURCEAOMNID = " + sourceAomnId +
                " order by TEHSECT";
    }

    ...

}
