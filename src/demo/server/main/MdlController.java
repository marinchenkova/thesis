@RestController
public class MdlController {

    private final RepositoryService repositoryService;

    @GetMapping("/aomn/list")
    public CompletableFuture<String> aomnList() {
        logger.info("Requested AOMN list");
        return repositoryService.aomnList();
    }

    ...

}