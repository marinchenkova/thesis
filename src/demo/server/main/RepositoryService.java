@Service
public class RepositoryService {

    private final MapperProvider mapperProvider;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RepositoryService(MapperProvider mapperProvider, JdbcTemplate jdbcTemplate) {
        this.mapperProvider = mapperProvider;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Async(AsyncConfig.EXECUTOR_REPO)
    public CompletableFuture<String> aomnNameById(int id) {
        String name = processWithTiming(() -> jdbcTemplate.query(
                SqlProvider.aomnNameById(id),
                mapperProvider.aomnNameResultSetExtractor()
        ));
        return CompletableFuture.completedFuture(name);

    }

    ...

}
