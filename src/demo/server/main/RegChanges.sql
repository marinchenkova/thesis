with full as (
    select
        tehsect.TEHSECTID     tehsect_id,
        tehsect.TEHSECT       tehsect_full_name,
        tehsect.SHORTNAME     tehsect_short_name,
        nvl(tehsect.TEHSECT, tehsect.SHORTNAME)
                              present_name,
        plan.dt               plan_date,             
        fact.dt               fact_date,             
        nvl(plan.dt, fact.dt) present_date,
        rank() over (partition by fact.dt order by fact.STATEDATE desc)
                              last_state,            
        rank() over (partition by tehsect.TEHSECTID, plan.dt order by fact.dt desc)
                              last_fact,             
        plan.FROM_REG         plan_from_reg,
        plan.TO_REG           plan_to_reg,
        plan.QCHANGEKIND      plan_q_kind,
        plan.QPREV            plan_q_prev,
        plan.QCUR             plan_q_curr,
        plan.CANCELED         plan_cancelled,        
        fact.FROM_REG         fact_from_reg,
        fact.TO_REG           fact_to_reg,
        fact.QCHANGEKIND      fact_q_kind,
        fact.QPREV            fact_q_prev,
        fact.QCUR             fact_q_curr

    from ASKIDADM.PDREGIMECHANGE plan

    full join ASKIDADM.PDFACTREGIMECHANGE fact
           on fact.PLANREGCHANGEID = plan.PLANREGCHANGEID

    join MFTEHAK.TEHSECT tehsect
      on (fact.TEHSECTID = tehsect.TEHSECTID or plan.TEHSECTID = tehsect.TEHSECTID)
         and
         (tehsect.PRODUCTTYPE = :productType)
         and
         ((select count(struct.TEHSECTID)
             from ASKIDADM.PDSTRUCTURE struct
            where struct.TEHSECTID = fact.TEHSECTID
               or struct.TEHSECTID = plan.TEHSECTID) > 0)

    where
        (plan.AOMNID = :aomnid or fact.AOMNID = :aomnid)
        and
        (plan.dt >= :dt and plan.dt < (:dt + 1) or fact.dt > :dt and fact.dt <= (:dt + 1))

    order by
        present_date asc,
        present_name asc
)

select
    tehsect_id,
    tehsect_full_name,
    tehsect_short_name,
    plan_date,
    fact_date,
    nvl(plan_from_reg, fact_from_reg) from_reg,
    nvl(plan_to_reg, fact_to_reg) to_reg,
    nvl(plan_q_kind, fact_q_kind) q_kind,
    round(plan_q_prev) plan_q_prev,
    round(plan_q_curr) plan_q_curr,
    round(fact_q_prev) fact_q_prev,
    round(fact_q_curr) fact_q_curr,
    plan_cancelled
from
    full
where
    last_state = 1
    and
    (plan_date is not null and last_fact = 1 or plan_date is null)
;