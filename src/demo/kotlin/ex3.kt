var a: String = "abc"
a = null               // compilation error

var b: String? = "abc"
b = null               // correct

b?.toUpperCase()