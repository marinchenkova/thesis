fun String.double(): String {
    return this + this;
}

// usage
val str = "Hello"
print(str.double())    // prints "HelloHello"