inline fun supportMarshmallow(code: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        code()
}

// usage
supportMarshmallow {
    print("This code will only run on Android Marshmallow and newer")
}

doAsync {
    var result = runLongTask() // runs on background
    uiThread {
        toast(result)          // runs on main thread
    }
}