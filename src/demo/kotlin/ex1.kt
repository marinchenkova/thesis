var name = "John" // Inferred type is String
name = "Bob"      // correct
name = 2          // compilation error