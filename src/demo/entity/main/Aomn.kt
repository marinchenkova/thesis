data class Aomn(
        @Expose val aomnId: Int,
        @Expose val name: String?,
        @Expose val shortName: String?
) : Jsonable()