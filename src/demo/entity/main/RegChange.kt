data class RegChange(
        @Expose val tehsect: Tehsect,
        private val planDate: Date?,
        private val factDate: Date?,
        @Expose val fromReg: String?,
        @Expose val toReg: String?,
        @Expose val qKind: QKind?,
        @Expose val planQPrev: Int?,
        @Expose val planQCurr: Int?,
        @Expose val factQPrev: Int?,
        @Expose val factQCurr: Int?,
        private val planCancelled: Boolean?,
        private val day: Date
) : Jsonable() {

    private val requestDate = Calendar.getInstance().time
    @Expose val isPlanNextDay: Boolean = !is24(planDate, day) && onNextDay(planDate, day)
    @Expose val isFactNextDay: Boolean = !is24(factDate, day) && onNextDay(factDate, day)
    @Expose val state: RegChangeState = initState()
    @Expose val planDateString = dateString(planDate)
    @Expose val factDateString = dateString(factDate)

    ...
}