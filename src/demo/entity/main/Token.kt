class Token {

    companion object {

        val AOMN: Type = object : TypeToken<Aomn>(){}.type
        val AOMN_LIST: Type = object : TypeToken<List<Aomn>>(){}.type
        ...

    }

}