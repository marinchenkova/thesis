open class DataGen {

    fun <A: Any> newVal(kClass: KClass<out A>, nullable: Boolean = true): A? {
        if (nullable && makeNull(nullProb)) return null
        return types(kClass)
    }

    open fun <A: Any> types(kClass: KClass<out A>) = when(kClass) {
        Boolean::class -> boolean() as A
        Int::class -> int() as A
        Long::class -> long() as A
        Double::class -> double() as A
        Float::class -> float() as A
        String::class -> string() as A
        Date::class -> date() as A
        else -> null
    }

    ...
}
