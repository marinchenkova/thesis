class JsonableFactory {

    private val dataGen = object : DataGen() {
        override fun <A : Any> types(kClass: KClass<out A>): A? = when (kClass) {
            QKind::class -> qKind() as A
            Tehsect::class -> newJsonable(Tehsect::class, false) as A
            else -> super.types(kClass)
        }
    }

    fun <J: Jsonable> newJsonable(kClass: KClass<out J>,
                                  nullable: Boolean = true): J? {
        if (nullable && dataGen.makeNull()) return null
        val ctor = kClass.primaryConstructor ?: return null

        val args = ctor.parameters.map {
            dataGen.newVal(it.type.classifier as KClass<*>, it.type.isMarkedNullable)
        }.toTypedArray()

        return ctor.call(*args)
    }

    fun newJson(jsonable: Jsonable?): String? {
        if (jsonable == null) return null

        val json = StringBuilder("{")
        json.append(
                jsonable::class.memberProperties
                        .filter { it.javaField?.getAnnotation(Expose::class.java) != null }
                        .filter { readProperty(jsonable, it.name) != null }
                        .joinToString(",") {
                            val value = readProperty(jsonable, it.name)
                            if (value != null) "\"${it.name}\":$value" else ""
                        }
        )
        json.append("}")

        return json.toString()
    }

    ...
}
