class JsonableTest {

    private val loops = 1000
    private val factory = JsonableFactory()
    private val types = listOf(
            Aomn::class,
            RegChange::class,
            Tehsect::class
    )

    @Test
    fun oneToJson() {
        types.forEach {
            for (i in 0..loops) {
                val entity = factory.newJsonable(it, false)
                val actual = entity?.toJson()
                val expected = factory.newJson(entity)
                assert(factory.jsonEquals(expected, actual))
            }
        }
    }

    ...

}

